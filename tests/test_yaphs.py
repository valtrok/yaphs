# -*- coding: utf-8 -*-

import sys
import pytest

from yaphs import Hook

class TestYaphs:
    def test_args(self):
        d = {
            'foo_args': None,
            'bar_before_args': None,
            'bar_after_args': None,
        }

        @Hook
        def foo(*args):
            d['foo_args'] = args

        @foo.before
        def bar_before(*args):
            d['bar_before_args'] = args

        @foo.after
        def bar_before(*args):
            d['bar_after_args'] = args

        foo(1, 2)

        assert d['foo_args'] == (1, 2)
        assert d['bar_before_args'] == (1, 2)
        assert d['bar_after_args'] == (1, 2)


    def test_order(self):
        d = {
            'i': 1,
            'foo_i': 0,
            'bar_before_i': 0,
            'bar_after_i': 0
        }

        @Hook
        def foo(*args):
            d['foo_i'] = d['i']
            d['i'] = d['i'] + 1

        @foo.before
        def bar_before(*args):
            d['bar_before_i'] = d['i']
            d['i'] = d['i'] + 1

        @foo.after
        def bar_after(*args):
            d['bar_after_i'] = d['i']
            d['i'] = d['i'] + 1

        foo()

        assert d['bar_before_i'] == 1
        assert d['foo_i'] == 2
        assert d['bar_after_i'] == 3

    @pytest.mark.skipif(sys.version_info < (3,), reason="requires python 3.x")
    def test_class(self):
        d = {
            'foo_args': None,
            'bar_o': None,
            'bar_args': None
        }

        class MyClass:
            @Hook
            def foo(self, *args):
                d['foo_args'] = args

        c = MyClass()

        @c.foo.before
        def bar(o, *args):
            d['bar_o'] = o
            d['bar_args'] = args

        c.foo(1, 2)

        assert d['bar_o'] == c
        assert d['foo_args'] == d['bar_args']